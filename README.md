# Matematicas AR - Andoria Labs. #
 

## Configuración Inicial ##
* Apache 2.4.47
* MariaDB 10.4.19
* PHP 7.3 o superior
* Unity 2020

### Frontend
Como tecnología frontend utilizaremos HTML, CSS, Javascript, jQuery Boostrap.
En donde, la documentación de estas últimas tecnologías las podemos encontrar en:
* jQuery: https://jquery.com/
* Bootstrap 5.0: https://getbootstrap.com/docs/5.0/getting-started/introduction/
### Backend
Como lenguaje de programación de lado de servidor  (Backend) utilizaremos PHP en su versión 7.3, donde ya es posible utilizarla luego de instalar el stack XAMPP.
#### Base de datos
Como motor de base de datos utilizaremos MySQL en donde se explica anteriormente en cuando se instala el stack XAMPP. 
#### Patrones 
Utilizar la tools del SDK
arcoreimg.exe build-db --input_images_directory=/path/to/images --output_db_path=/path/to/myimages.imgdb
### Convenciones de programación y nombramiento de variables
#### Variables y atributos de clases
Los nombres de las variables se utilizará la nomenclatura CamelCase, en donde consta de los nombres de clase con las primeras letras de cada palabra y a los objetos y otras variables, serán utilizadas con la primera letra de la primera palabra en mayúscula, seguidamente por la segunda palabra empezando con mayúsculas.
Podemos obtener más información en el siguiente link:
https://sites.google.com/site/edmundoogaz/buenas-practicas-java
#### Clases y tablas de base de datos
Las clases y las tablas de la base de datos arrancan con la primera letra en mayúscula y en el cambio de palabra será colocado un “_” comenzando así la próxima palabra en minúscula la primera letra. Ejemplo: Usuario o Marco_teorico
Para los atributos de la Tabla se generará con un acrónimo de la tabla y el nombre del atributo separado por “_” . Ejemplo para la tabla Usuario usu_id , usu_password, usu_nombre
#### Análisis
Para poder llevar a cabo el análisis del producto, se utilizara la herramienta StarUML para poder realizar los diferentes diagramas de clases y los diagramas de entidad relación.
Donde lo podemos descargar de la siguiente página sin registro:
https://staruml.io/
